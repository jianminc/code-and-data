## load packages
library(ggplot2)
library(data.table)
library(lubridate) ## for dealing with variables of Dates
library(dplyr)
library(maps)  
## need to use BiocManager to install Biobase before installing the NMF package
##if(!requireNamespace("BiocManager", quietly = TRUE)){
##  install.packages("BiocManager") 
##}
##BiocManager::install("Biobase")
library(NMF)
library(gridExtra) ## for grid graphics
library(tidyr) 
library(fpc)  ## for producing ARI of clustering results

states_map <- map_data("state")
us_list <- unique(states_map$region)

## read data from online source: 
##url_confirmed <- 
## "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/
##csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_US.csv" 
##dat_confirmed <- fread(url_confirmed, na.strings = c("", "NA"))

## A local version of the github data was saved on Oct.5 to 
## avoid later change from the source. 
## Our analysis is based on this local data to get reproducable results. 
dat_confirmed <- fread('data_10_05.csv', 
                       na.strings = c("", "NA"))
dat_confirmed <- dat_confirmed[, -1]

## data table transform: only keep the data for 49 states (including Washington D.C.)
## remove unused columns
dat_confirmed <- dat_confirmed[, -c(1:6, 8:11)]
dat_confirmed <- dat_confirmed %>% 
  filter(tolower(Province_State) %in% us_list)

## get state level data from county level data
dat_confirmed_sum <- dat_confirmed %>% 
  group_by(Province_State) %>% 
  summarise_each(sum)
statename <- dat_confirmed_sum$Province_State

## get daily counts
nmfgap <- matrix(0, nrow(dat_confirmed_sum), 
                 ncol(dat_confirmed_sum) - 2)
for(i in 3:ncol(dat_confirmed_sum)){
  gap <- unlist(dat_confirmed_sum[, i] - dat_confirmed_sum[, i - 1])
  gap[which(gap < 0)] <- 0
  nmfgap[, i - 2] <- gap
}

# get population for each state
state_pop <- read.csv("./nst-est2019-01.csv", 
                      skip = 3, sep = ",", header = TRUE)
state_pop <- state_pop[1:56, ]
names(state_pop)[1] <- 'state'
state_pop$state <- tolower(gsub('\\.', '', state_pop$state))
state_pop <- state_pop[, c(1,13)]
state_pop$X2019 <- as.numeric(gsub(",", "", state_pop$X2019))
names(state_pop)[2] <- 'population'

## rescale confirmed counts by state population
dat_confirmed_sum$Province_State <- 
  tolower(dat_confirmed_sum$Province_State)
datmatch <- dat_confirmed_sum %>% 
  left_join(state_pop, 
            by = c('Province_State'='state'))
population_w <- 1/datmatch$population

## get confirmed counts per 1 million population
wmatrix <- diag(population_w)*1000000
nmfgap <- wmatrix %*% nmfgap

## Run 7-day simple moving average
fdat <- matrix(0,nrow(nmfgap),  ncol(nmfgap))
fdat[, 1:3] <- nmfgap[, 1:3]
for(i in 4:(ncol(nmfgap) - 4)){
  fdat[, i] <- 
    apply(nmfgap[, c((i - 3):(i + 3))], 1, mean)
}


## use data from 3/22 to 7/25
## set incremental gap at 7 (days)
starts <- '3/22/2020'
ends <- '7/25/2020'
datelist <- mdy(names(dat_confirmed)[-1])
startdate <- which(datelist == mdy(starts))
lastday <- which(datelist == mdy(ends))
dlength <- lastday - startdate + 1
gap <- 7
nperiod <- ceiling(dlength/gap)

