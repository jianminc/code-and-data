This folder contains the code and data.

The working directory needs to be set under the 'code' folder 
and the 'manuscript' and 'code' folders should be put under 
the same root folder.

pretreat.R: Get data and do pretreatment(smoothing and scaling) on the data. 

getparameter.R: Get NMF ranks by cross-validation. Need to source in pretreat.R. Runing this file may take a relatively long time. Hence, the selected NMF ranks are written as fixed values in model_fit.R. 

model_fit.R: Fit final NMF models. Select number of groups for kmeans clustering and get final clustering results. Need to source in pretreat.R

plotmaking.R: Get ARI for each study periods. Make corresponding plots for the paper. Need to source in model_fit.R. Figures generated are saved under the 'manuscript' folder.

data_10_05.csv: data save on Oct.5th, 2020

nst-est2019-01.csv: state level population data



