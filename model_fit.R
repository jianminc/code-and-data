## Source pretreat.R
source('./pretreat.R')

## r: NMF rank, obtained from getnmfparameter.R
r <- c(2, 5, 4, 6, 6, 8, 7, 8, 8,
       7, 8, 8, 12, 11, 12, 11, 12, 12)

## get NMF coefficients for each period with selected rank in r
## store coefficients in a list
clusters <- list()
for(i in 1:nperiod){
  nmfdat <- fdat[, c((startdate -1 ):(startdate + gap*i - 2))]
  w <- matrix(1, ncol(nmfdat), nrow(nmfdat))
  resfit1 <- nmf(t(nmfdat), rank = r[i], method = 'ls-nmf', 
                 seed = 'nndsvd', weight = w)
  fitcoefs <- coef(resfit1)
  clusterd <- t(fitcoefs)
  clusters[[i]] <- clusterd
}

## for each value of k (group number), do multiple runs to get the global minimum 
## save the min of WSS(k) and the corresponding seed
getwss <- function(coefs, maxk){
  minwssk <- NULL
  minkseed <- NULL
  wssall <- NULL
  dmat <- dist(coefs)
  for(k in 2:maxk){
    wss <- rep(0, 500)
    for(i in 1:500){
      set.seed(i)
      kresult <- kmeans(coefs, k, iter.max = 10)
      wss[i] <- kresult$tot.withinss
    }
    minwssk <- c(minwssk, min(wss))
    minkseed <- c(minkseed, min(which(wss == min(wss))))
    wssall <- c(wssall, kresult$totss)
  }
  return(data.frame(minwssk, minkseed, wssall))
}

optimalk <- list()
for(i in 1:nperiod){
  optimalk[[i]] <- getwss(clusters[[i]], 10)
}

## get the optimal k via elbow plot: min(WSS(k)) vs k 
for(t in 1:nperiod){
  plot(2:10, optimalk[[18]]$minwssk, type = "b", 
       xlab = 'k', ylab = 'WSS' )
}

## get final clustering result, save in groups
groups <- matrix(0, nrow = 49, ncol = length(r))
# optimal k values saved in k1
k1 <- c(5, 5, 5, 6, 5, 5, 6, 6, 6, 
       6, 7, 7, 8, 8, 7, 7, 7, 7)

seeds <- rep(0, length(k1))

## get corresponding optimal seed from minkseed
for(i in 1:nperiod){
  seeds[i] <- optimalk[[i]]$minkseed[k1[i] - 1]
}

## get clusters
for(i in 1:nperiod){
  set.seed(seeds[i])
  kresult <- kmeans(clusters[[i]], k1[i], iter.max = 20)
  groups[, i] <- kresult$cluster
}

## get avg coefs for each cluster
coef18 <- data.frame(clusters[[18]])
coef18$cluster <- groups[, 18]
avgcoef <- coef18 %>% group_by(cluster) %>% 
  summarise_all(funs(mean))
## assign the cluster names matching the paper
avgcoef$cluster <- c('F', 'C', 'B', 'G', 'E', 'D', 'A')


